import numpy

def costFunction( Valor ):
    try:
        return ((Valor - 5)/2)**3 + (Valor - 5) + numpy.heaviside(Valor - 8,0)*(numpy.exp(Valor - 8) - 1)
    except:
        return "Value not a number"


def aura ( dic_valores , dic_pesos = {}, dic_porcentajes = {}):
    "Toma como argumento dos diccionarios y devuelve un float"
    result = 0
    # Si no han introducido pesos
    if len(dic_pesos) == 0:
        for criterio in dic_valores:
            result += costFunction( dic_valores[criterio] )
        return round( result , 2 )

    # "Si hay pesos"
    # "Vectorizo los datos"
    pesos_vec               =               numpy.array(list(dic_pesos.values()))
    porcentajes_matriz      =               numpy.array(list(dic_porcentajes.values()))
    relaciones_vec          =               numpy.dot(porcentajes_matriz,pesos_vec)
    valores_vec             =               numpy.array(list(dic_valores.values()))
    
    for i in range(0,len(valores_vec)):
        result += (numpy.e/10) * relaciones_vec[i]  *   costFunction(   valores_vec[i]  )

    return round( result,2 )

def softmax ( valor,  vector_de_valores ):
    """
    Calcula la transformación softmax de un valor sobre un grupo de valores
    Toma valor como número y vector_de_valores como lista de números    
    """
    denominador = 0
    for v in vector_de_valores:
        denominador += numpy.exp(v)
    
    numerador = numpy.exp( valor )

    return numerador / denominador


def aura_multi_option ( lista_dic_valores , dic_pesos = {},dic_porcentajes = {}):
    """
    Aplica Aura a una lista de opciones
    """
    result = []
    
    for dic_valores in lista_dic_valores:
        result.append( aura(dic_valores, dic_pesos, dic_porcentajes) )
    return result



def aura_soft_multi_option ( lista_dic_valores , dic_pesos = {}, dic_porcentajes = {} ):
    """
    Toma una lista de diccionarios con los valores de cada opción
    Usa la transformación softmax para devolverte la probabilidad con la que deberías hacer cada opción
    Devuelve un diccionario con las puntuaciones de cada opción
    """
    
    # Creo el diccionario con el valor máximo para escalar los resultados
    val_max = 9    
    
    dic_max = {}
    for criterio in lista_dic_valores[0].keys():
        dic_max[criterio] = val_max
    norm_factor = aura( dic_max, dic_pesos, dic_porcentajes)

    result_list = aura_multi_option( lista_dic_valores, dic_pesos, dic_porcentajes )

    # Normaliza los resultados
    for i in range( len(result_list) ):
        result_list[i] = result_list[i] / norm_factor   
    
    
    probabilidades = {}
    for i in range( len(result_list) ):
        probabilidades["option " + str(i+1)] = ( round( softmax( result_list[i], result_list) ,2) )
    
    return probabilidades