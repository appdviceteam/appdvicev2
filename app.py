from flask import Flask, render_template, request, session, redirect, url_for   # Import the class `Flask` from the `flask` module, written by someone else.
from AuraV4 import aura, aura_multi_option, aura_soft_multi_option
from json import dumps
from datetime import timedelta

app = Flask(__name__)                               # Instantiate a new web application called `app`, with `__name__` representing the current file == coje el nombre del archivo entiendo.
app.secret_key = b'_7#y2L"F4Q8z\n\xec]/'            # Se utiliza para encriptar las sesiones
app.permanent_session_lifetime = timedelta(minutes = 15)

criteria = ['Cost','Difficulty','Risk/Uncertainty','Fun','Benefit','Trascendence','Impact on relationships']
questions = [
    ["question 1","Does it require many resources (time, money...)?","Few","Lots"],
    ["question 2","How challenging are the actions this option entails?","Easiest","Hardest"],
    # ["question 3","Is there risk involved? How well defined are the consequences?","Risky / Uncertain","Safe / Crystal clear"],
    ["question 3","Is there risk involved? How well can you predict the consequences?","Safe / Crystal clear","Risky / Uncertain"],
    ["question 4","How much fun is it?","Not at all","Very enjoyable"],
    ["question 5","How possitive would the impact in your life be?","Terrible","Most possitive"],
    # ["question 6","How much closer to your dreams does this decision take you?","Inconsecuential","Trascendental"],
    ["question 6","Does this get you closer or further from your dreams?","Furthest","Closest"],
    ["question 7","Will it affect people you care about?","Most negatively","Most possitively"],
]

questionWeightRatio = {# Al implementar las sesiones descubrí que no estaban bien porque se me ordenan los pesos alfabéticamente. Tuve que compensar, por eso ya no son la matriz identidad
# Ordenación que me pone:{'Benefit': 50, 'Cost': 50, 'Difficulty': 50, 'Fun': 50, 'Impact': 50, 'Risk/Uncertainty': 50, 'Trascendence': 50}
    'pregunta1':[0,1,0,0,0,0,0],
    'pregunta2':[0,0,1,0,0,0,0],
    'pregunta3':[0,0,0,0,0,1,0],
    'pregunta4':[0,0,0,1,0,0,0],
    'pregunta5':[1,0,0,0,0,0,0],
    'pregunta6':[0,0,0,0,0,0,1],
    'pregunta7':[0,0,0,0,0,1,0],
    }

@app.route("/")                                     # A decorator; when the user goes to the route `/`, exceute the function immediately below
def index():
    return render_template( "index.html" )

@app.route("/Why/")                                     
def why():
    return render_template( "why.html" )

@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('index'))
    else:
        return '''
            <form method="post">
                <p><input type=text name=username>
                <p><input type=submit value=Login>
            </form>
        '''

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))

@app.route("/Decisions/", methods = ['POST','GET'] )
def decisions():
    try:
        # for key in request.form:
        #     session["weightsDic"][key] =  int( request.form[key] )  
        return render_template( "decisions.html", 
        
        weights_html = session["weightsDic"],
        questions_js = dumps(questions),
        
        )
    except:
        return render_template( "decisions.html",

        questions_js = dumps(questions),

        )

@app.route("/Weights/", methods = ['POST','GET'] )
def weights():
    if request.method == 'POST':
        session["weightsDic"] = {}
        for key in request.form:
            session["weightsDic"][key] =  int( request.form[key] )      # el diccionario dentro de session se ordena alfabéticamente
        return redirect(url_for('decisions'))
    else:
        return render_template( "weights.html" , criteria_html = criteria)

# def weights():
#     if 'weightsDic' not in session:
#         session["weightsDic"] = dict()
#     return render_template( "weights.html" , criteria_html = criteria)

@app.route("/Result/" , methods = ['POST','GET'] )
def result():

    # Copio el InmutableMultiDict a un diccionario normal para trabajar con él
    userAnswers = {}
    print("request form: ",request.form)
    for key in request.form:
        userAnswers[key] = request.form.getlist(key)
    print( "userAnswers :", userAnswers)
    optionsList = userAnswers['option']

    lista_dic_valores = []
    for i in range(len( optionsList )):
        dic_valores = {}
        counter = 1
        for question in list( userAnswers.keys())[1:] :
            if counter <=3:     # Invierto los valores de las tres primeras preguntas
                dic_valores[question] = ( 100 - int(userAnswers[question][i] ) ) /10
            else:
                dic_valores[question] = int( userAnswers[question][i] ) /10
            counter += 1
        lista_dic_valores.append(dic_valores)
        
    # weightsDic = session["weightsDic"]
    # dic_valores.pop('option')
    # if len(weightsDic) != 0:
    if "weightsDic" in session and len( session["weightsDic"] ) == len(criteria):
        result = aura_soft_multi_option( lista_dic_valores, session["weightsDic"], questionWeightRatio)
    else:
        session["weightsDic"] = "No weights"
        result = aura_soft_multi_option( lista_dic_valores )
    


    return render_template( "result.html",  

    Output1_html                                =       userAnswers                 ,
    Output2_html                                =       lista_dic_valores           ,
    Output3_html                                =       session["weightsDic"]                  ,
    Output4_html                                =       result                      ,
    Output5_html                                =       questionWeightRatio                      ,

    graphData_html                              =       list( result.values() )     ,
    graphLabels_html                            =       list( optionsList )         ,  
    )


@app.route('/service-worker.js')                            # Ruta para servir el archivo service-worker.js
def sw():
    return app.send_static_file('service-worker.js')

if __name__ == "__main__":
    app.run(debug = True)
    app.run()