const CACHE_NAME = 'static-cache';

const FILES_TO_CACHE = [
    // './static/offline.html',              // Página que se sirve al usuario cuando se está offline. Por alguna razón ha de estar en static
    // './static/indexCopy.html',              // Efectivamente, no funciona intentar cargar archivos de templates y en cuanto hay un error, no se carga nada.
]

// Install event                    
        // On install this code opens the cache you declared previously (CACHE_NAME) and then caches 
        // the files you want caching (FILES_TO_CACHE). 
self.addEventListener('install', (event) => {
    console.log('[ServiceWorker] install event');
    event.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            console.log('[ServiceWorker] Pre-caching offline page');
            return cache.addAll(FILES_TO_CACHE);
        })
    );

    self.skipWaiting();
});


//   Activation
        // Activa el service worker y limpia cachés antiguas
self.addEventListener('activate', (event) => {
    console.log('[ServiceWorker] Activate');
    event.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (key !== CACHE_NAME) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key);
                }
            }));
        })
    );
    self.clients.claim();
});

//   Fetch
    // The fetch event listener allows the service worker to work as a middleman 
    // between your web server and the browser
self.addEventListener('fetch', function (event) {
    event.respondWith(fetch(event.request));
});

//  No network -> offline page
self.addEventListener('fetch', (event) => {
    if (event.request.mode !== 'navigate') {
        return;
    }
    event.respondWith(fetch(event.request).catch(() => {
        return caches.open(CACHE_NAME).then((cache) => {
            return cache.match('offline.html');
            // return cache.match('index.html');
        });
    })
    );
});

// Network failing -> Back to cache
self.addEventListener('fetch', function (event) {
    event.respondWith(
        fetch(event.request).catch(function () {
            return caches.match(event.request);
        })
    );
});

// Cache falling back to network
self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request).then(function (response) {
            return response || fetch(event.request);
        })
    );
});