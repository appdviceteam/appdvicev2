var rangeSlider = function(){
    var slider  =   $(`.range-slider`),                    // Selecciona el div
        range   =   $(`.range-slider__range`),              // Selecciona el range
        value   =   $(`.range-slider__value`);              // Selecciona el span donde se registra el valor
    
    slider.each(function(){

        value.each(function(){
            var value = $(this).prev().attr('value');
            $(this).html(value);
        });

        range.on('input', function(){
            $(this).next(value).html(this.value);
        });
    });
};

rangeSlider();
