//@ts-check

var counter = 1 // Indexing starts at zero in JS

function count(){counter = counter + 1};

// Aquí puedo meter html si me interesase usando las comillas ``
// var questions = [
//     ["criterion 1","questionText 1","lowendText 1","highendText 1"],
//     ["criterion 2","questionText 2","lowendText 2","highendText 2"],
//     ["criterion 3","questionText 3","lowendText 3","highendText 3"],
//     // ["criterion 4","questionText 4","lowendText 4","highendText 4"],
//     // ["criterion 5","questionText 5","lowendText 5","highendText 5"],
//     // ["criterion 6","questionText 6","lowendText 6","highendText 6"],
//     // ["criterion 7","questionText 7","lowendText 7","highendText 7"],
// ];

function createQuestion(criterion,questionText,lowendText,highendText, optionNumber) {         // Voy a meter la variable adicional optionNumber aquí también para poder diferenciar
    var questionHTML = `
                        <div class = "range-slider${optionNumber} hover"> 
                            <h3>${questionText}</h3>
                            <div class="row">
                                <div class="col-2">${lowendText}</div>
                                    <div class="col-8"> 
                                        <input class="range-slider__range${optionNumber}" name="${criterion}" type="range" value = "50" " min="0" max="100">
                                        <span class = "range-slider__value${optionNumber}">0</span>
                                    </div>
                                <div class="col-2">${highendText}</div> 
                            </div>
                        </div>
    `
    return questionHTML;
};

function createOption(optionNumber){
    var option = `             
            
            <label for="checkbox${optionNumber}" for="Option${optionNumber}">
                <input type="checkbox" id="checkbox${optionNumber}" class = "checkbox" data-toggle="collapse" data-target="#option${optionNumber}" aria-expanded="false" aria-controls="option${optionNumber}">
                <svg class="bi bi-chevron-compact-right rotate" width="1.5em" height="1.5em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path class = "rotate" fill-rule="evenodd" d="M6.776 1.553a.5.5 0 01.671.223l3 6a.5.5 0 010 .448l-3 6a.5.5 0 11-.894-.448L9.44 8 6.553 2.224a.5.5 0 01.223-.671z" clip-rule="evenodd"/>
                </svg>
            </label>

            <input type = "text" value = "Option ${optionNumber}      " name = option ${optionNumber}">

            <div class = "collapse" id = "option${optionNumber}">
                <div class = "card card-body">
                `

    for (let index = 0; index < questions.length; index++) {
        option += createQuestion(questions[index][0],questions[index][1],questions[index][2],questions[index][3],optionNumber)
    }
                
    option += `</div>
               </div>`
    
    return option;
};


function addOption() {
    // @ts-ignore
    $("#contenedor").append(createOption(counter));  // El append de Jquery sí que parece funcionar.

    rangeSlider();                                  // Esta función está definida en otro archivo -> RangeSliders.js
    count();
};

var addOptionButton = document.getElementById('AddOption');
addOptionButton.addEventListener('click', addOption); // Parece que no se le pueden pasar argumentos usando esta forma

/*
// Parametrizaciones iniciales
addOption();                                            // Para que aparezca creada la primera opción
//Para que aparezcan desplegadas las preguntas de la primera opción
document.getElementById('Option1').setAttribute('aria-expanded','True'); // toco el botón
document.getElementById('option1').setAttribute('class','collapse show'); // toco el div

*/